#pragma once
#include "cPais.h"

class cJugador
{

public:
	cJugador(string nombre);
	~cJugador();
	void Atacar();
	void GanarPais(int numeroPais);
	//void operator-(int paisReceptor, cTropas* tropaAenviar);
	void MoverTropas(int paisReceptor, cTropas* tropaAenviar);
	//cJugador& operator=(int pais);
	void PerderPais(int numeroPais);
	void SumarTropas(cTropas* tropaReceptora, cTropas* tropaEmisora);
	friend class cJuego;
private:
	int cantPaises;
	const string nombre;
	cLista<int> paisesPropios(MAX_PAISES);

};

