#pragma once
#include "cClase.h"
#include "cTropas.h"

class cArquero : public cClase
{

public:
	cArquero();
	~cArquero();
	int atacar();
	void recibirdanio(int& AT);
	void operator-(int& AT);
};

