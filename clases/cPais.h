#pragma once
#include "cTropas.h"
#include "clista.h"

class cPais
{

public:
	cPais(string nombre);
	~cPais();
	void reacomodarTropas();
	bool operator=(cPais* pais_a_comparar);
	bool operator!=(cPais* pais_a_comparar);

private:
	int cantTropas;
	cLista<cTropas>* listatropas;
	const string nombre;
	static int num;
	bool ocupado;

};

