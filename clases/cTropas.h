#pragma once
#include "cClase.h"
#include "cLista.h"
#include "string"
using namespace std;
class cTropas
{

public:
	cTropas();
	~cTropas();
	int Atacar(cTropas* TropaAtacada);
	//cTropas* operator !=(cTropas* tropaAtacada);
	cTropas* operator= (cTropas* TropaNueva);
	int operator()();
	cTropas* operator+(cTropas* TropaEmisora);

private:
	//Cant de unidades en cada tropa depende de la clase
	int cantTot;
	int cantUnid;
	string Tipodeclase;
	cLista<cClase>* tropa;

};

