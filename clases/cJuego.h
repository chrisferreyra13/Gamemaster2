#pragma once
#include "cJugador.h"
#include "cPais.h"
#include "cLista.h"
#include <defines.h>


class cJuego
{

public:
	cJuego();
	~cJuego();
	static void Ataque(cPais* PaisAtacante, cPais* PaisDefensor);
	void iniciar();
	static void MoverTropas(cPais* PaisCede, cPais* PaisRecibe);

	//Cambia de turno, jugador
	void operator++(int);

	void terminarJuego();

private:
	//El operador++ suma uno a jugadorjugando y turno ejecuta el turno del jugadorjugando, lo llama el operador++
	void turno();
	static int cantActJug;
	int jugadorjugando;
	bool estadoPartida;
	cLista<cJugador>* jugadores;
	static cLista<cPais>* listaPaises;

};

