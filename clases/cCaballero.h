#pragma once
#include "cClase.h"
#include "cTropas.h"

class cCaballero : public cClase
{

public:
	cCaballero();
	~cCaballero();
	int atacar();
	void recibirdanio(int& AT);
	void operator-(int& AT);
};

