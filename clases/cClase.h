#pragma once
class cClase
{

public:
	cClase();
	virtual ~cClase();
	virtual int atacar()=0;
	virtual void recibirdanio(int& AT)=0;
	virtual void operator-(int& AT)=0;

protected:
	int AT;
	int Def;
	int HP;

};
